+++
title= "About"
+++
Hi! 

I am a professional silk weaver on a quest to regenerate my craft. 

![Weaving velvet](weaving-velvet.jpg)

I decided to pursue a Master’s degree in Contemporary Design to think about my practice and try to find solutions to support it. For a year at Aalto University, I dedicated myself to discovering other topics and expressing myself through “foreign” materials, to provoke an estrangement that would shift my perspective from the conventional track. Nurturing my intuitions on how to be a hand-weaver in the XXIth century.  

This broader perspective guided me towards joining the Digital Fabrication Course to learn a whole new set of skills. 