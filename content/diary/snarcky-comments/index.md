+++
title= "Acid reflux"
+++


[week one](#25012023)

[week two](#01022023)

[week three](#08022023)

[week five](#23022023)


## 23/02/2023

## Well WELL, 

What a discovery this week. Being a diligent student, I connected on the Zoom meeting for the global lecture earlier. And I heard… WHAT… *instructors* have their own **instructor project.**

## So it never ends. 

If you are **IN** then you need to go back in the loop of suffering for half a year, every year? Additionally to your duties as teacher and supervisor of course. 

Of course at that point you already became an Adrenaline Junky. 
### Oh Neil! 

This is madness. 

### Oh KID ! 

*This kid has Tourette syndrome and wants to create a device that engages policemen leading to a safe situation.*

The first person in this class who has an actual purpose and a cause to follow through his project, his class, his life. That is comforting at last. 


>*I wonder... How long can a person dedicate 130% of himself before breaking down and going to live in the forest growing vegetables?*

>*I wonder... How many of the students in this program are truly seeking for validation? Me included.*

---------------------------------------------------------

## 08/02/2023

### Oh Neil! 

This time I might get something from your advices. This is really cool 

[Thank you Cornell University!](https://digital.library.cornell.edu/collections/kmoddl)


### Oh Neil! 

You encourage everyone to be more ambitious with their project, make it bigger, make it more complex, make it with cool colour features… 

**Bigger, harder, stronger**

### Oh Neil! 

*That very cool artist has a CPU problem because probably a shitty computer*

You found her super cool tools this Ncurses might be really fun. 
Then you insits on the fact that you have no idea how to solve her CPU problem, but you know the developer of the program and will see him today! Do you think he can offer her a new computer? 

>*I wonder...* about those engineers and developers who are thinking that reminding you to breathe with alarms and buzz would help you to manage your levels of stress. 

>*I wonder...* a human size running wheel… Is there any projects that can have a practical real-life purpose in this program… 
I hear the make “anything” is taken in the very literal sense... 

---------

## 01/02/2023


### Oh Neil! 

*That kid is having a problem with his computer running on windows*

One more tool to master... "Put a Linux shell on windows, kid."

Easy Neil. We are not going to become developers from one day to the next. 

That kid is not struggling enough do you think? Let him enjoy life. See many other moose in the forest. More time IRL, less in front of his computer! 

### Oh Neil! 

*That kid struggles to learn piano. He wants to make an automatic piano to have a companion and be challenged to learn the piano himself*

Tell this kid how much your friends are geniuses. You know the most gifted piano player in the whole world. See, he will feel really good about their own difficulties to learn the piano... 

### Oh Neil!

Now you are showing fast forward all the possible CAD tools in the universe. With all the possibities available... Is the goal to show people how to use it? Or showing people you can master them? 
Is anyone supposed to be able to follow this fast pace? 

>*I wonder...* How many people in this course are guided by a will to be part of Neil's cast? The winners. How far the philosophy of this goes to lift up the losers to a point where they can believe they are insiders? 
Cheat them to feel that by paying several thousand dollars, they will get skills to be in the winning team. To invent some cool shit. To support their lives better. 

>*I wonder...* How many people in this course are lonely, isolated and looking for being part of something? An adventure. A team. Some excitement. How much this is pushing towards their loneliness because they are faced with unexpected problems they are struggling to solve. How difficult it can be to reach out for help. In a big mass of more than a hundreds. Especially if you don't have it in the shape of a human being, in your near environment. A teacher, a friend. 

--------

## 25/01/2023

First "Global class"
Today we were introduced to the concept of FabAcademy. An organisation ruled online from the office of the mighty MIT Professor Neil. Like an octopus, it has many legs and brains that stretch away from the main head. And it reaches the farthest corner of the earth.

This is how it works: members pay a consequent fee between 5000 and 10 050€ to participate and receive a diploma. Then, they can become instructors, try to open a local "node" and recruit a new class of participants.

Basically, it is a sect. Neil is the Gourou. It works like every pyramidal system of influence. You pay to enter, then you are inside, and you grow the community to feed the Octopus. Making the main head bigger and bigger.

So big, it is really surprising it can pass doors...

The hybris.

In the shape of a highly educated white middle-aged man. 

What a cliché… 

From now on I will invoke the Gourou as one should:

### Oh Neil!

Neil talks about creating "life from scratch". He can grasp the complexities of creation. Of civilisation; "designing reality". He fashioned a toolkit compiling the minimal equipment to recreate a civilisation on Mars.

We are following Aalto’s class structure. I am observing the Octopus, deploying his tentacles and happily swimming in the water. We are a rather remote tentacle. 

### Oh Neil!

Neil is proud of the diversity he could foster. 

He believes with creating one ubiquitous formula, one master toolkit he can provide solutions for all.


>*I wonder… Where is the ethics? Did the head cut the tentacle? 

>Probably the one baring humility never grew.

![Octopus Neil](neil-octopus.jpg)

---------