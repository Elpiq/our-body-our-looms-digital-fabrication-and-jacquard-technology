+++
title= "Check-ins"
+++

[Check in - 10/02/2023](#check-in---10022023)

[Check in - 03/02/2023](#check-in---03022023)

[Meeting Valerie](#first-steps-on-git)



The most impressive human trait, adaptibility. Getting used to any situation. Who can survive are the ones who accept diligently the load. Who can be stretched.

--------------------------------------------------------

## Check-in - 10/02/2023

Friday, 1:00am.

Now I could take the time to develop what I am really interested in. Looking for information on looms, mechanical engravings from encyclopedias, records of mechanical innovations in technical journals… Thrilling perspective. 

Or I could just go to sleep. 

5 hours later, wake up, prepare, commute and in front of the computer again. With a bitter sensation that I didn’t do my best. Because I wanted to do more and better. But I couldn’t. The will is here, vivid. Not only in me, but in the other students that are joining this class as well. We are all facing this illusion: yes you can! Other students did it before you. 
And, there is this urban legend: Nedieh.
The scale of excellence to compare your abilities with. A dreadful perspective. 

Why is it that we have to compare ourselves? To be framed on a scale, a movable cursor with comparison points. They are all very relative while pretending to be absolute. It leaves no room for independence, for following one’s specific path, with one’s own ability. For some people drawn towards a competitive mindset, it can be motivational. 

* “marche ou crève” (litterally walk or die)*

For the others, we can decide not to participate. Not to pretend to be in the race. It is a choice to make. A deliberate movement to step aside. Let the others run, while you are watching the flowers along the road, and feeling the fresh air on your skin. 
It requires a lot of courage. First because in the eyes of the one’s still running, you gave up. You are the loser. 
Assuming this posture, being watched and judged for it is not pleasant. 
Then because it means you are on your own. 

Is there any space left to think? To consider. If you are kept so busy that you cannot look within. Are we condemned to constantly run? Pretend we can win this race? Then one could say, it is not about winning but running, being in motion, and participating. Are we condemned to be excluded if we don’t fight? If we cannot compete? 

*this is the fabric from which slavery is born*

Is there no other way to participate? 

My own desires are in friction with these considerations. I am a hand-weaver. I deliberately chose some shoes in which it is impossible to run. Some old, worn-out shoes abandoned in some dusty basement. Some shoes belonging to a museum. 
Those shoes are fitting my feet so well. They are very clever and beautiful. 


## Check in - 03/02/2023

Hard to express how I feel about all this now. 
I am tired that is for sure. 
It is the first time in weeks that I allow myself to check in, to take a necessary distance to consider what happened, and how I feel. 
The first sensation that comes to my mind and to my body is confusion. I am confused. It is hard to assess any of the tasks I need to do, assess the time it will take, and assess what I want from it. 

And then I get lost in my confusion. I am sketchy and the hierarchy between things disappears. I end up spending time digging too deep, towards things I am not able to comprehend yet, which are not necessary for the task I am required to provide now. 

This leads to a second overwhelming feeling: frustration. 
Here I can only see the signs but not the meaning. It is as if I could access only vision, and cannot hear, smell or touch. A very partial apprehension of the scene. It is flat.

And I am handicapped. 

Learning is a process of unravelling layer after layer the depth of an object. A pile that has been build by centuries of activity. 

I like to dive into a new subject, have the freedom to explore, to make connections with other objects. It builds a balance made of curiosity, humility, and joy. Learning makes me want to learn more, it teaches me the depth of my ignorance, and the knowledge other people hold. The satisfaction from making links, building new bridges between bodies.

I believe one of the reasons why beings have been teaching each other is to guide people through a path to unravel the layers. 

There is a kind way: showing the first visible layer and explaining it, building safe supports to reach the second, third layers below. Holding hands and watch. Then practice and practice alone. A slow and dedicated progression. Solid.

Together 

There is a brutal way: grabbing a random visible corner of the pile and pulling it hard. Then examine what lies in your hand. Panicking. Looking at all the others pieces which crumbled down. Laying on the ground, they lost their order. Can you even identify what they are? There from the layer you hold, you can try to retrace a structure. Many attempts, many failures. You are fighting. Not knowing where to start, where to go. Holding on tight to every object you can bring together, assembling many pieces of puzzles that cannot match. 

Alone

Freedom comes from the hand being held. 

It makes me think about teaching strategies. The recent development, accelerated by covid to provide lectures online. Direct students toward tutorials, emphasizing the infinite depth of the available knowledge on the web. The power of self-learning, the pride of being an “auto-didact”. 
There are no limits to what one can learn. 

I would say this is a dangerous illusion. 

*A dramatic red flag arose in my mindbody saying: this emphasizes a mindset for an individualistic society. It is promoting the figure of the self-made man. Selfishness and egotistic behaviour. What if it ultimately guides schools to disappear? Are teachers soon going to be outdated?*


As human beings, we are finite and have limited time and limited possibilities. We need others to guide our paths. Being physically present to each other. Learning from all our senses. In volume.

*Well not for long, wait till Elon Musk implant you a chip in the brain, put you in the matrix while he transforms himself into a cyborg*

I see somehow a link with a familiar situation. What happens if knowledge is rare? If it is intangible? Does it exist in such a system? Is it valued? 

I am used to this format of hysteric work. It matches my fire. But I know how it can burn; consuming all the flesh, stepping out the brazier and starting to burn the house down.

Or how it can build a fortress of loneliness. 

Did it for one full year in Japan. Took several years to recover from it. I am still unravelling the teachings from this time. Probably will do it for the rest of my life.

Is it the only way though? Is there not a more caring and soft way to learn.

The thought of quitting brushed my mind several times. But then my body in a wave of love... 

I see her... In her crust of dusty grease. 

I want to do this for her. For me, for us. 
So we can both continue weaving. 
And we can teach others too. 

I will focus on that pile from now on. 
Hell to the other piles. 

--------


## Meeting Valérie

15:12 Whatever I do, I cannot ask git to change the directory. 

![]()img crazy one 

All the manipulation I had to do, to overcome git not finding my folder.
I tried every possible things, the simple copy of the path would not work. I copied the path myself. Tried with respecting the caps, without it… nothing. 

I tried checking what path he could understand. Desktop was the limit to it. I decided to use the command to create folders. Creating the proper sub-folder and initializing the directory each time. It proved successful. 

After nearly two hours of fight… I could succeed this. I still don’t understand what went wrong, but I know how to cheat it. And I learn several more commands on the way. 

I used the **git add** command to add my index file, and had no severe reactions… 

NOPE 

It is still taking all the files around as possible directories: 

![Crazy GIT 3](crazy-git-three.jpg)

It is still taking all the files around as possible directories: 

![Crazy GIT 4](crazy-git-four.jpg)

And more 

In the midst of all this… I see the “new file” I want… Index.html. 
He is telling me to use some obscure command to “unstage”... Let’s check what this mean. 
It means, it should be excluded from further commit… 

**BUT NOOOO**

When I ask for a commit, both with and without using the path of this file that is what I get 

![Crazy GIT 5](crazy-git-five.jpg)

Every time I close and reopen github… It is like nothing happened… 
Though the files have been created on my desktop… And all the commands previously executed… I am dying here. 

![Crazy GIT 6](crazy-git-six.jpg)

17:00 I am utterly desperate right now. I will stop for today! 

17:37 Still trying to look for solutions on forums, I cannot let it go… But I really should. Now is the time.

17:41 uhhhhhhhhh

17:45 I need a cookie.

17:51 As usual, because my brain is a Broadway stage, I have a perfect soundtrack for the moment: Valerie… Amy Winehouse.

“Well sometimes I go out by myself
And I look across the water
And I think of all the things, what you're doing 
And in my head I paint a picture [..]
Won't you come on over 
Stop making a fool out of me 
Why don't you come on over Valerie?
Valerie? Valerie? Valerie?”

{{< youtube bixuI_GV5I0 >}}

So, it is decided that GIT is officially renamed VALERIE ! The fussy Valerie… 

I am done, Valerie! 

21:46 Home. Tired.
Hopefully Takashi talked a bit of sense into Valerie. And taught me how to communicate with her. 











