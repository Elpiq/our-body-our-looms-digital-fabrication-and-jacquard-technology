+++
title= "Final Project"
+++



Last year, la Maison des Canuts, a small private museum on the hill of Croix Rousse offered me a Jacquard mechanic machine. It took some time, but I could organize a project to bring this mechanic back to life. I want to question the dynamics and our behavior towards technology and innovation. The political reasons behind the uprooting of our traditional and intangible knowledge and culture. For a year now, I have been planning this project with the Workshop Master of the Fablab in Aalto. 

I decided to flirt with the other side and learn digital fabrication technics to bring my loom into the XXIst century. I will be replacing his usual punch card programming with a digital piloting device. The Jacquard mechanics machine  will be computerized while being operated by hand. While studying for the FabAcademy I will reflect and document the effects it has on my analog body and soul. 


During my stay in Lyon, I could organize the shipping of the machine to Finland. I received a grant from the Design Department to this end and invested some personal funds. 

## First step 

Give it a step! 

I realised I cannot start to take care of the mecanics and clean it in its actual state. It need a step mecanism to open and close the grid... 

While on a loom it would look like this.

![Looks like this](jacquard-2.jpg)

*In Prelle's workshop*

![Looks like this](jacquard-0.jpg)









