+++
title= "History"

+++

Textile has been world the leading market guiding a global process of industrialization. Towards the XVIIIth and XIXth century Lyon’s region used to be the world reference for silk weaving. It underwent a fast decay with the advent of mechanization. Redering obsolete the need for hand-made complex fabrics. 

In both resistance and cooperation towards the system that was about to cause their loss, The “Canuts”, Lyon’s independant silk weaver and workshop masters, were favoring virtuosity, developing their skills and tools further to push the boundaries of their craft. Educated, and politicized, “Canuts” were at the forefront of mutualism and republican spirit. 

Nowadays, less than 10 hand-weavers mastering specific silk techniques are active in Europe. They are located in France and Italy. Most of them focus on historical reproductions and work on original XIXth looms with XXth century Jacquard mechanic machines. 

I used to work for the last family owned silk Manufacture in Lyon: Prelle. 


## Standing on the shoulders of Giants 


From the 1970s, historical Lyon silk companies went bankrupt. The activity of hand-weaving already minor, permanently collapsed. The remaining workshops were dismantled and most of the looms and tools for hand-weaving were discarded. Some items could be left in museums.

Throughout the whole 20th century, thousands of hand looms have been thrown away, used as a wood fire, dismantled for metal parts, etc… To leave room for modern tools for industrial production. 

sub-menus:

On mechanics, 
On Joseph-Marie Jacquard, 
On intellectual property, 
On politics, 
On Canuts, 
On Music Machines, 

