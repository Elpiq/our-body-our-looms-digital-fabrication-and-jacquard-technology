+++
title= "3D printing"
+++

[Blender Geometry Node](#blender---geometry-nodes)

[Step by Step - Fractal Design with Blender](#step-by-step---fractals-with-blenders-geometry-node)

[First steps on the printer](#first-trial-on-the-printer)

[Experimentations](#experimentations)

> [I.](#i)

> [II.](#ii)

> [III.](#iii)

> [IV.](#iv)

> [V. To be continued](#v)

Before starting this task, I asked myself:

>> *“what kind of 3D-printed shape I could create that would be difficult to make by hand?”.* 

It was a hard question as I believe and support that, with the right skills and patience **we can do anything by hand.**
Though I have been fascinated by the shapes that nature can produce, using mathematical rules. It is amazing how beautiful a snowflake looks, or the way flower petals are multiplying and distributed. Inspired by this, I thought it would be nice to use a mathematically generated shape. Something hard to create by hand on a small scale. 

### Blender - Geometry Nodes 

I tried a bit of Blender before… 

Before the new type of user interface. I gave up rather quickly… It was a dreadful experience, and I was a bit traumatized. 
One of my classmate, **Vihar Kotechka**, is a Blender Genius. I have seen him work on his digital sculptures. He taught me several things that made me realise that you need an amazing understanding of the phenomenon you will use. To play with light, for example, you need to understand how light will behave inside a room, on a surface… otherwise what you will create will look “wrong”... 

![Vihar blender hero](vihar.jpg)

I asked him to coach me using Blender geometry node tool. 
We watched this tutorial together 

[watch the tutorial here](https://www.youtube.com/watch?v=bHWvVtuLJkM&ab_channel=CrossMindStudio)

*The steps are exctracted from the tutorial and should be credited to Youtube Channel Cross Mind Studio.*

Then I watched Vihar creating the fractal pattern on the cube. He explained that lacking information every step. I tried to watch him carefully. 

Then on my own I tried to follow the steps. 

### Step by step - fractals with Blender’s Geometry Node 

We are starting from a plane cube. 

![starting point](blender-0-0.jpg)

Then we from the **add menu** or **shift+a** with the pointer in the group zone we select **mesh** then **extrude mesh** 

![mesh menu](blender-0-2.jpg)

Here a new menu will appear and we place it between the **input** and **output** box the **geometry** will automatically connect to the **mesh** point. 

![mesh box](blender-0-3.jpg)


And the cube will already change aspect: 

![Cube with mutiple faces](blender-0-4.jpg)

I will save this first iteration as a stl file. I would like to print the step by steps iterations of this cube. 

Then add a new node **Shift+a**, **mesh** select **scale element**

![cube](blender-0-5.jpg)

The **scale elements** nodes permit to select a parametered scale for all individual elements of the shape if necessary. 

We will select the output **top** and connect it to the **selection** pin of the **scale elements** node. 

![link top and selection](blender-0-6.jpg)

That will only scale the top side of the extruded mesh 
We will change the scale of the **extrude mesh** to 0.1 and the **scale elements** to 0.6

![scaling](blender-0-7.jpg)

Select the 2 nodes and press **ctrl+g** to make it a group. Then press **tab** to collide both in a smaller menu node. Call it **extrude and inset**

![group nodes](blender-0-8.jpg)

We will now multiply the effect, it will multiply the faces and add details to the shape. 
We could use a **loop** node or simply multiply the freshly create node. That is what we will do. Using **shift+d**. 

![multiply node](blender-0-9.jpg)

All of these new elements are connected and sharing properties. We will reopen the 
**extrude mesh** node and change the **offset scale** to **0.01** to obtain very detailed fractal ornaments on the cube. 

![scale offset](blender-0-10.jpg)

![details of ornaments](blender-0-11.jpg)

We will continue changing the faces and adding some details to the ornamentation by copying the **extrude mesh** node 

Add a new node **face area** and plug it into **selection** of the new **extrude mesh** node. 

![details of ornaments](blender-0-12.jpg)

![details of ornaments](blender-0-13.jpg)

We will add a **compare** node to trigger some of the bigger faces only. They will be extruded again. Compare **greater than**, choose **0.4**. 

I don’t know why… I don’t have the same amount of details than the tutorial shows…

Here Vihar pointed out, I did not duplicate the **scale elements** as well. 

![details of ornaments](blender-0-14.jpg)

![details of ornaments](blender-0-15.jpg)


## First trial on the printer

The first test was on the Ultimaker S3 printing a sample for testing the “finishing”. I thought it would be interesting to see the level of detail on the surface we can reach. 

I opened the file on **Cura** on the lab computer. 

First select a printer. 
We can select the different printer here: 

![Cura select printer](Cura1.jpg)

![Select printer](Cura2.jpg)

We can select the quality of the print: **Fine 0.1** would be a good enough quality, it rather takes some time. If we just need to make tests we can decide on other faster quality. 

![Select printer](Cura12.jpg)

I was advised by Hiski to change some parameters: we can change the **infill** structure for example to optimize the time. 

![Change parameters](Cura3.jpg)

![Change infill](Cura4.jpg)

Also, we should change the **”built adhesion plate”** parameters. 
Untick the **“Enable prime blob”**, and choose the **Built plate adhesion type **skirt**. 

![Change adhesion plate settings](Cura5.jpg)


We can use the side panel menu in the **Prepare** mode, to control the scale: 
First click on the object. That enable the menu.

![Change scale](Cura7.jpg)

Being careful to keep ticked the **uniform scaling** box to keep the ratios intact. Although if we want to experiment with scales and distortions, that would be a good way… 

Or orientation of the object: 

![Change orientation](Cura8.jpg)

Once all the parameters are set. Press **Slice** 

![Change scale](Cura6.jpg)

We can go to the **Preview** menu and check every slice of the printing process. 

![Change name of file](Cura10.jpg)

The software propose a tool to save automatically the file on an external drive. It is better to name the file properly before saving it. 

![Change name of file](Cura11.jpg)

We can also eject the flash drive automatically on the software. 

## First Impressions

I launched the print and saw the offset of the skirt was not properly parametered. The skirt is not separated from the object itself. But it doesn’t really matter for this. 

The print went very well and is super precise. I am impressed. Also I didn’t expect it would take such a big amount of time…

I was a mesmerized by the printing process, and kept watching it... 

{{< video src="printfinishing.mp4" >}}


## Experimentations

I saved all the phases of the geometry node transformation of the cube element. I tried to print some of them as 3D objects. 

I am eager to try out the textures of the test I have made. 

## I.

I used the Ultimaker 2+ connect 

When reaching the second area of the shape, I realised that the design would need some supports. I stopped the print and changed the parameters to add supports. 

![Lacking supports](fail-0.jpg)

I started printing again, and realised the scale was actually too small and the print wouldn’t proceed and warped.  

![Scale too small](fail-1.jpg)

I set the scale to 500%. And restarted it. 

Then it printed on very well. I like this shiny pink of course. 

I decided to keep the supports on to illustrate how the shape was built. 

![Cube1 final](handcube-0.Jpg)

{{< video src="printfirsttest.mp4" >}}

## II. 

![Cube scale](cube2-0.jpg)

![Cube slice](cube2-1.jpg)

The second cube, turned out well! 

I scaled it to 800% this time. It is still rather small. 

The surface is really clean and shiny. 

While taking it off the plate. I realised it also would have needed some supports as the bottom is not totally flat. 

The building layers are a bit sketchy as it was not totally flat. 

![Cube2 final](handcube-1.Jpg)

## III.

The most interesting cube I wanted to try was this one: 

![Cube3 cura](cura6.jpg)

I am very curious how the texture and details of the volumes will turn out. I decided to use the Ultimaker S3 to be able to have as smooth details as possible. I realised the S3 was a bit more precise and sharp on the details than the S2+ connect. 

I processed the file on Cura, but somehow the printer was showing a **conflict** on the monitor. Kris helped me to set the file properly again. And we decided to try using the 2nd extruder on the machine. 

>>*This machine has two extruders that can print with two different materials. One of the extruders is typically used for the main material, while the other is used for a support material that can be dissolved or removed after printing.*

We then set the 2nd Nozzle to PVA. 

![select 2nd nozzle](cube3cura-0.jpg)

That action enabled more options in the **support** settings. We selected  **extruder n2** to print exclusively the supports.

![select extruder 2 supports](cura7.jpg)

![select extruder 1 for the built plate](cura8.jpg)

Something was wrong again. We realised we had to change the PVA parameters to BB 0,4, instead of AA 0,4. 

Why? I asked **Chat GPT**  who says: 

>>*The "BB" in PVA BB stands for "Breakaway/BVOH", which means that it is a blend of PVA and BVOH (butenediol vinyl alcohol co-polymer), another water-soluble material that can be used as a support. This blend is designed to provide better adhesion between the support material and the build plate, making it easier to remove supports without damaging the print.*

On the monitor of the printer remained a **conflict** warning, but we decided to ignore it and proceed with the printing. 

{{< video src="cube3.mp4" >}}

It took 2 hours for a cube of 2cm! 

![lonely spot](cube3print-0.jpg)

I am getting lonely and impatient. I brought Shrodinger cat (thank you Kris) for company, and some snacks.

The PVA material was not extruding well and would create error messages very frequently. But it seemed to provide enough support to go on with the printing so I decided to continue with it. Continuously pressing the button on the monitor which was telling me PVA run out, while it was still there and could be extruded. I kept hearing bubbles exploding and see it looking nasty. My conclusion was that either the material has some problems, either the temperature of the nozzle is incorrect. But I couldn’t change it anyway. So as long as the PLA looked good to work on it… 

![finally cube3](cube3print-1.jpg)


The cube turned out nicely. The definition of the ornaments are not as precise as in the model. I believe the scale is to small to obtain such a great level of details and the depth it is suppose to have. But I cannot print an a bigger scale for now. I don’t want to rob my colleagues some precious printing time. 

![Change name of file](handcube-2.Jpg)

I decided not to wash the PVA to keep it as an example. 

## IV. 

![cube4 blender](blender-0-15.jpg)

The level of details on the last step of the fractal cube is too thin and elaborated. On a small scale it would appear like blurbs.

I decided to test the structure and the ornaments instead, creating this miniature cristal sculpture. 

![oddcube blender](oddcube-0.jpg)

I distorted the cube shape to try and see what type of odd structure the printer will be able to print in this small scale without the need of building support. 

Once more I used the PVA material to support the base and keep the ornaments on all sides. 

The print will take about 2 hours as well and I try to keep it small enough. 

![oddcube cura](oddcube-1.jpg)

![oddcube printer](oddcubeprint-0.jpg)

Tough the odd structure could be printed without any problems, the ornaments were not nice. It turned out all **BLURBY**. I am a bit disappointed. 

![oddcube printer](oddcubeprint-1.jpg)

{{< video src="oddcube.mp4" >}}

I washed the PVA away this time. Cutting the pulp of my finger on the sharp edge of the bumps. Crystals ha… 

![oddcube printer](handcube-3.Jpg)

## V. 

To be continued… 

Now I understood this ornamental crystals could be printed and nicely created in a bigger scale. Then I will take a loooooong time to print. It is better that I reserve my own time for it. 

I would like to try it one more time with the S3 printer abbout 5 times bigger.

And I started to apply the texture on a free model of Pine cone that I found on [**thingyverse**](https://www.thingiverse.com/thing:3616238)

For now it looks like this. 

![oddcube printer](blenderpinecone.jpg)

I hope I can try to use the sla printer to see how this texture turns out. Some examples in the lab showed some long self supported spikes. I am hoping to go that direction. 

I realise this type of geometry is very interesting to avoid extra supports, while giving beautiful ornemental and detailed textures. 

Nature is the best architect! 

The lab in Vare just acquired some wood based filament. The texture is similar to cork, and that could be something nice to try out as well. But I have now idea of design for that at the moment...



## WTF is this? 

>>WTF is **PLA**? 

>>>>PLA stands for Polylactic Acid, which is a biodegradable thermoplastic polymer made from renewable resources such as corn starch, sugarcane, or tapioca roots. PLA is one of the most commonly used 3D printing materials due to its ease of use, low toxicity, and eco-friendliness.
PLA is a popular choice for 3D printing because it is relatively easy to print with, has a low printing temperature compared to other materials, and can produce high-quality prints with a smooth finish. It is also available in a wide range of colors and can be used to produce various objects such as toys, household items, and even medical implants.
In addition to being biodegradable and made from renewable resources, PLA is also compostable under the right conditions, meaning it can break down into natural elements without causing harm to the environment. This makes it an excellent choice for sustainable and eco-friendly manufacturing processes.

Source: **CHAT GPT**









