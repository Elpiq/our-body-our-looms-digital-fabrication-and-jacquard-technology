+++
title= "Embedded programming"
+++

[Resources](#resources)

[Installation of the Arduino Software](#installation-of-the-arduino-software)

[Soldering](#soldering)

[Test Blink and other examples](#test-blink-example)

[Serial/UART](#serialuart)

[Main structure and basic script decipher](#main-structure-and-basic-script-decipher)

[Experimentations](#experimentations)

[Pickles](#pickles)

[WTF is this?](#wtf-is-this)

[videos](#videos)

### Resources 

We will be using Seeed Studio XIAO RP2040

Install the Arduino software [here](https://www.arduino.cc/en/software)

[Seeed Wiki page](https://wiki.seeedstudio.com/)

[Seeed Studio XIAO RP2040 with Arduino wiki page](https://wiki.seeedstudio.com/XIAO-RP2040-with-Arduino/)

[Arduino Variables cheat sheet](https://www.arduino.cc/reference/en/#variables)

[Xiao seed RP2040 PIN OUT](resource1.jpg)



### Installation of the Arduino software 

Browse the board package for our XIAO RP2040 

![arduino board choice](arduino-0.jpg)

Install the so called Raspberry Pi Pico/RP2040

![arduino board download library](arduino-1.jpg)

Choose the board **seed XIAO RP2040** Plug the usb on the arduino and choose a **port**. 

![](arduino-4.jpg)

### Soldering 

I asked advice from Yu-Han on how to proceed and what to pay attention to and asked her to demonstrate before starting. 

I sat on the weird saddle chair, very unstable… Started the vent, and put on the other soldering iron, tested the soldering iron in front of the vent. It was already set to 320°c. 

And there I went! 

![](soldering1.jpg)

![](soldering2.jpg)

## Tadah!!! 

![](soldering3.jpg)

![](soldering4.jpg)

### First steps

Ok… If I understood correctly. 

### Main structure and basic script decipher

>> **()** 
>>>> specification of the function parameters.

>> **{}** 
>>>> Description of what the function does.

Don’t forget **spacing**, the **;**

>>**pinMode**
>>>> refers to the specific pin in which send the information and configure as OUTPUT or INPUT. 

>>>>>ex: void setup() {

>>>>> pinMode(13, OUTPUT);

>>>>Here illustrated, the **pinMode** needs two **arguments** to be defined. 

>>>>The **pin** argument: it configures the **pin number** you are choosing refering to the **pinout** map, 

>>>>The **mode** argument can be either INPUT or OUTPUT.

The **pins** D0 to D10 are multipurpose. And they can be used for any input or output if applied current to it.

[all about pinMode](https://www.arduino.cc/reference/en/language/functions/digital-io/pinmode/)

>>**Input**
>>>>Sends the information to the device. 
>>>>An input can come from a variety of sources either inbuilt or added devices, such as sensors, switches, buttons etc...

>>>>For ex, if you connect a push button to a digital **INPUT** pin on the Arduino board, the button will send a signal to the board whenever it is pressed. The Arduino can then read this signal using the **digitalRead()** function and perform some action based on the button being pressed.

>>**Output**
>>>>Gives instructions to operate any connected device. An output can be used to control a variety of devices, such as LEDs, motors, displays, or other electronic components, etc...

>>>> For ex, if you connect an LED to a digital **OUPUT** pin on the Arduino board, the board can control the LED by sending a digital signal to the pin using the **digitalWrite()** function. 

>>**HIGH**
>>>> function to **turn off** (applies for the XIAO board)

>>**LOW**
>>>> function to **turn on** (applies for the XIAO board)


>>**void setup**
>>>>Tells what action to operated when the Arduino board is powered on or reset. It is indicating any settings or variables: configuring **pins**, setting the baud rate for **serial communication**, to be executed **once**.

>>>>>Ex: int powPin = NEOPIXEL_POWER; 

>>>>>void setup(){

>>>>>>pinMode(powPin, OUTPUT);

>>>>>>digitalWrite(powPin, HIGH);
}

>>>>Here we set up an **int** we called powPin to define the **NEOPIXEL_POWER**. Then in the **void setup** we defined the actions using the **pinMode** and **digitalWrite** functions. 

>>**void loop** 
>>>>controls the occurrence of the action after the **setup** function has been executed. It is where you can write the main logic of your program. As its name state it, in command the action as a **loop**, repeatedly. 

>>>>>Ex: void loop() {

>>>>>digitalWrite(powPin, LOW);

>>>>>delay(1000);

>>>>>digitalWrite(powPin, HIGH);

>>>>>delay(1000);
}

>>>>Here the **digitalWrite** function is telling the inbuilt LED to turn on with a **delay** function defining to wait 1 sec. Then **digitalWrite** again for turning off the LED and **delay** to wait 1 sec af. 


### Test:  blink and other examples. 

![](arduino-2.jpg)

![](arduino-3.jpg)

It works ! 


Trying the examples from the class video. I don’t understand a thing! 
But at least it works.

![](arduino-6.jpg)

![](arduino-7.jpg)

![](arduino-8.jpg)



Using the NeoPixel LED 

Makings experiments on the **delayms** value. **ms= millisecond**, this means that the higher the number, the slowest the blinking occurrence will be. 

What if I put **s** instead for second? 


Apparently not that straightforward… 

### Serial/UART

A way of communicating between computers. Two separate actions: one cable sending data, the other receiving data. On the Arduino, specific pins are dedicated to **UART**: **RX and TX**. 

Documentation about **serial** [here](https://www.arduino.cc/reference/en/language/functions/communication/serial/)

![Serial.begin](serial.0.jpg)

I asked **Chat GPT** to explain the different between bits and bytes: 

>>*Bits and bytes are both units of measurement in digital computing, but they represent different things.*

>>*A bit is the smallest unit of measurement in digital computing. It is a binary digit, meaning it can only have one of two values: 0 or 1. Bits are used to represent the most basic level of information in computing, such as the on/off state of a switch or the presence or absence of a signal.*

>>*A byte, on the other hand, is a larger unit of measurement that is used to represent a group of bits. Specifically, a byte is a group of 8 bits, which means it can represent 2^8 or 256 different values (0-255). Bytes are used to represent larger units of information, such as a single character or a small amount of numerical data.*

>>*In summary, a bit is the smallest unit of measurement in digital computing, while a byte is a larger unit of measurement made up of 8 bits.”*


**Baud value** should be selected somewhere **between 9600 and 115200** suited to this Arduino board. It changes the speed at which the communication will be transmitted. higher value, higher speed. 


![Serial monitor](serial.1.jpg)

The baud value should bet set the same as determined by the Serial.begin, here **19200**.

![Serial monitor](serial.2-0.jpg)

Here if we decipher this line by line: 

![Serial monitor](serial.2-1.jpg)

>>the **char message = Serial.read();** line reads a single character from the serial port and define it as a message. **char** indicates that message is a **character data type**. 

>>the **Serial.print("Received: ");** line sends "Received: " to the serial port. **Serial.print()** function is used to send data to the serial port, and in this case it's being used to send text.

>>the **Serial.println(message);** line sends the value of the **message variable** here defined by **char message = Serial.read();** to the **serial port**.


Here, he Arduino board will read a single character from the serial port, store it in the message variable, and show "Received: " in the serial port monitor. 

![Serial monitor](serial.2-2.jpg)

We can then assign the **message** a language for the single character to be interpreted. **DEC** for decimal, **BIN** for binary. 

Report to the **ASCII table** [here](https://www.ascii-code.com/). 

### Functions

>>**int**
>>>> Stands for “integer”, it defines a “data type”, that you can reuse in your code later on. 
>>>> find more about **int** Integers are your primary data-type for number storage.


>>**bool**
>>>>Specifies if on or off, and helps the code to act accordingly.
>>>>The syntax is: bool variable = value; the value here is **true** or **false**.
>>>>>ex: 

>>>>>**bool button1Pressed = false;**

>>>>>**bool button2Pressed = false;**

>>**millis()**
>>>>Chat GPT says: “You can use the millis() function to create delays in your program without blocking the execution of other code. The traditional delay() function is a blocking function, which means that it stops the program execution until the delay time is completed. This can be problematic when you want to execute other code during that time, like reading sensor inputs or processing user inputs.”
>>>>Here is an example of how to implement it:

![](expe10.jpg)

![](expe11.jpg)

![](expe12.jpg)

Then I asked for explanations line by line...

![](expe13.jpg)

![](expe14.jpg)

![](expe15.jpg)

![](expe16.jpg)

![](expe17.jpg)

>>**char**
>>>>Used for converting a character into a **data type**
>>>>ex provided by ** Chat GPT **
>>>>>**char myChar = 'A';** // stores the character 'A' in the variable myChar
>>>>>**Serial.print(myChar);** // prints the value of myChar to the serial monitor

>>**Serial.println()**
>>>>Send data to the serial port.

### Experimentations

#### *What if I want the LED to show a specific RGB colour?*

Can I simply assign it a RGB reference? 

![code RGB](expe1.jpg)

![code RGB in Arduino](expe2.jpg)

It seems it works very well. 

__________________________________

#### *Can I play with the buttons?*

Here I will try to install two different buttons controlling each one with a different colour for the neo pixel LED.

Well first of all, I need a bigger bread board. 

![new bread board](new.board1.jpg)

![int buttons](expe4.jpg)

![void loop button](expe5.jpg)

I defined two buttons and their Pin: btnPin1, btnPin2

Fixing the code: 

I struggled a lot with trying to find the right structure for it. Yu-Han helped me to change a few things. For example, assign the RGB as an **int** value. 

![fixing the code](expe6.jpg)

![fixing the code](expe7.jpg)

>**”Serial.println("btn1");** to write a comment on in the serial monitor indicating which button is pressed. 


And it works!!! I am so proud 

---------------------------------------------------

#### *What if I want to blink the LED simultaneously*

Well... not that easy apparently... 

hmmm somehow it doesn't know which action to choose from. 

Maybe it might be because I didn’t assign the loop an end. 

Let’s see… 

After researching forums I discovered the **bool** data value, to track the button 1 or 2 being pressed. As such:

>>**bool button1Pressed = false;**

>>**bool button2Pressed = false;**

Then I asked **Chat GPT** how it works. 

>>*“When one of the buttons is pressed, its respective flag variable is set to true and the other flag variable is set to false. Then, inside the loop, the code checks which flag is true and executes the code for that button.”* 

says Chat GPT.

I reorganised the code of the loop separating the actions and gathering the **integers**

Here is the possible action for the signal to be sent: 

![fixing code in loop ](expe8.jpg)

Here is the possible action for the buttons: 

![actions if buttons](expe9.jpg)

But still… Something is weird… It seems the button cannot activate easily the colors, the color comes in a very random occurrence when pressing the button multiple times. But the Buit-in LED works just fine. 

There is still some command blocking the Neopixel LED to operate well. 

Could it be this **delay**? 

Let’s ask Chat GPT for his help fixing my code… 

>>*”I see. It seems that the issue is that the **delay** function is blocking the code execution and preventing the buttons from being read while the LEDs are blinking. One way to solve this is by using non-blocking code using the **millis()** function.”*

## YES ! 
Good instinct on this…

He provided me with some modifications in the code: 

![chat gpt](expe7.jpg)

![](expe8.jpg)

>>*”In this updated code, the blink function is using the **millis()** function to create a **non-blocking delay**. This allows the code to continue executing and reading button inputs while the LED is blinking.”*

It seems to have fixed the button problem. Though now the built-in LED doesn’t blink alternatively anymore.

And I am wondering what is this **millis()** function. And how to use it…

So I continue asking Chat GPT for explanations...

WELL… This goes beyond my comprehension at the moment… 

So I will stop the experimentation here. 


### Pickles: 

Why there is no port selection on my menu ?!??? 

![pickle port selection](pickle.arduino-0.jpg)

Aaah I need to plug it IN! Then it can show a port……. 

—-------------------------------

What does it mean “button” is not determined in that scope? 

![pickle typo](pickle.arduino-1.jpg)

Let see, because it is not supposed to be “button”... but “btnState” 

---------------------

Many script error, but at least he is kind enough to propose a solution! 

![typo fix with arduino](pickle.arduino-2.jpg)

### WTF is this? 

>WTF is a **“pin out”**? 

>>>>Refers to the diagram that illustrates the layout and function of the pins on an Arduino board. Each pin serves a specific purpose and can be used to connect to different components, sensors, or other devices.

>WTF means  **“Integers”**? 

>>>> “Integers are your primary data-type for number storage.” 

>source (https://www.arduino.cc/reference/en/language/variables/data-types/int/)


>>>> hmmmm… not helping… 

>WTF is the difference between **INPUT”** and **INPUT_PULLUP**? 

>>>>Find good explanation with examples [here](https://roboticsbackend.com/arduino-input_pullup-pinmode/)

### Videos 


{{< video src="random-colour.mp4" >}}

{{< video src="alternate-colour.mp4" >}}

{{< video src="fix-brightness.mp4" >}}

{{< video src="not-working.mp4" >}}

{{< video src="led-purple-green.mp4" >}}

{{< video src="serialbtn.mp4" >}}








