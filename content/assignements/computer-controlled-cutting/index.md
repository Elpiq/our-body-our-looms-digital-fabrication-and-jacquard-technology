+++
title= "Computer-controlled cutting"
+++

[How to use of the laser cutting machine](#how-to-use-of-the-laser-cutting-machine)
>[First steps on the laser cutter](#first-steps-on-the-laser-cutter)

>[Calculate the Kerf](#how-to-calculate-the-kerf)

[Finger joint box](#finger-joint-box)
>[New tools in Fusion](#new-tools-in-fusion)

>[Steps of Making](#steps)

>[From Fusion to Illustrator](#from-fusion-to-illustrator)

[WTF is this?](#wtf-is-this)

[Pickles](#pickles)

## How to use of the laser cutting machine

The file for printing will be opened on illustrator. It should be saved in a format supported by the program: SVG, AI, PDF… 

The first step is to open the vent.

Then use the key card to activate the laser cutter machine from the controlling device. 

Install the material and level the head of the machine to match the material. 

First bring the head to the bottom right corner using the axis menu on the controller.

![level laser](lasercut-0.jpg)

![level laser](lasercut-1.jpg)

Then open the focus menu

![focus menu laser](lasercut-2.jpg)

And bring down the metallic thingy, elevate the machine bed till forming an angle.

![Metallic thingy](lasercut-3.jpg)

Then bring it down till the thingy is straight and barely touching the material. 

![metallic thingy](lasercut-4.jpg)

All set! 

### On the computer

Open the files on illustrator, check that points are joined correctly. Then select all the cutting lines and set the thickness to **0,01**. It will be what the machine recognizes as to cut. 

If any engraving make sure they are vectors. 

Then select file/print/


Don't forget to choose **custom** in the **Media size** menu. Otherwise the machine software will not recognize the design. 

![media size custom](lasercut-5.jpg)

The machine software opens automatically. 

Drag the shape and install it where it should be cut on the sheet, be mindful of saving material as much as possible. 

Select the desired parameters on the right top menu. Pre-set parameters exists for all the materials, they can be tuned to what you want to achieve. 

All set, send to **job manager**

![send to the JM](lasercut-6.jpg)

Go on the job section, select the desired file and send it to the machine. 

![job manager](lasercut-7.jpg) 

On the control pannel of the machine, select the job and press the play button. 

And it is cutting! 


### First steps on the laser cutter
In the Lab with Damla, we practised how to use the laser cutter machine. 

![Damla on the computer](lasercut-8.jpg)

No designs yet. We decided to play with the parameters. Using the 3mm cardboard, we played with the **power** of engraving to asses which colours we might obtain. 

We tried 35%, 50% and 70%. 

![control panel](lasercut-9.jpg)

The default setting is 35% for 3mm cardboard, it results in a fade grey tone. 
While the 70% setting burn the material, the 50% setting looked the most striking. 
It gives me a better idea of the shades we could play with using different power of engravings probably between 30 and 60%. We could probably get a pallet of 6 tones. 

We then made kerf experimentations, changing the laser strenght and assessing if it had an impact on the kerf. After finding a method to measure the cutted square and making the calculations. We concluded that for 3mm cardboard, the strenght level had no impact on the kerf. 

![](lasercut-10.jpg)

![](lasercut-11.jpg)

### How to calculate the kerf 

Make a patter of 5 square of 2x2cm. Cut those square from the material to test. 

Put those square aligned on a piece of paper, and mark dots at each extremity of the segment. Measure the segment from the dots, using a caliper or a simple ruler. 

The calculation to perform is: 

>>5x20=100

>>100-size of the obtained segment /5

Ex: 
>> 100-98.3=1.7mm

>> 1.7/5= 0.35

0.35 is the kerf. 

We will need to redimension our design taking in account this measurement. 


## Finger joint box 

I know my way around illustrator -ish. So I will stick to fusion this week to make the 2D drawing for the box.

### New tools in Fusion: 

>>**User Parameters**

The user parameter menu helps to define parameters of measurements that can be modified later on to alter the modelling. It can be used for height, width, length and thickness, parameters of a material etc. Also as a formula to give a certain ratio ex: 5% of width…

>>>> Modify/change parameters/user parameters

 ![Parameter settings](image-0.jpg) 

 ![Parameter settings](image-1.jpg) 

Instead of typing the measurement, we will type the reference of the parameter.


Same for the extrusion, we can type the reference of the parameter. Here **cardboard**. It will automatically extrude with the thickness of the 3mm cardboard. 

![Parameter use](image-3.jpg) 

>> **Rectangle from the centre point**

>>> It will draw the rectangle using the centre point of the plane. Very handy tool for mirroring and symmetries

>> **X** 

>>>> shortcut to transform a line in a construction line. 

>> **ESC**

>>>> shortcut to get back to the selection tool.

>> **P** 

>>>> Project command: It allows us to use a constructed surface to “project”, draw a plane in a different orientation. Instead of drawing all the planes separately, It builds on the existing plane. 

>> **Mirror** 

>>>> This tool is replacing the lines and shapes drawn in a mirror effect. 

>>>>First select the objects

>>>>Then choose a plane corresponding to the direction to mirror and replicate. 


### Steps 

First enter the dimensions needed in user parameter, here I chose a square bottom of 120mmx120mm and 200mm height for the panels. I chose the 3mm cardboard. 

If I want to change, the material, or dimension of the same design, later on, it will be very handy! 

Draw a centre point rectangle and enter the measurements width and length

Then create the finger joints rectangles. use construction lines for the spacings. Make them perpendicular and equal. Select the bottom parts of all rectangles and make them equal as well. Then choose one rectangle long section and decide a length dimension, enter the parameter of the width as “cardboard”. 

![First step](image-4.jpg) 

![First step](image-5.jpg) 

Ready to mirror all this to the other side. Select the segments to duplicate and then choose the desired plane.

![mirror](image-6.jpg)



Replicate the actions to the other segment of the rectangle. 

![replicate the tabs](image-7.jpg)

Mirror it as well 

![mirror the second side](image-9.jpg)

Then press finish sketch. 

Extrude, select the inside part of the plane, enter “carboard” as the thickness of the extrusion. 

![extrusion settings](image-10.jpg)

And here we have the bottom of our box!

![finished bottom](image-11.jpg)

Now we can draw the side panel of the box. 

Create a new component and create a sketch. Select the side of the object as a surface to draw from. 

![choose a plane to draw sketch](image-12.jpg)

**P** for project, select all the elements of the visible section to project using all of the pieces. 

![select the surface to project](image-13.jpg)

Create a rectangle, and repeat the first steps to create the finger joints on this surface.

![draw rectangle on the projected sufrace](image-14.jpg)

![draw the tabs](image-15.jpg)

Mirror, selecting the section of the finger joint to mirror and the plane. 

![choose plane to mirror the tabs](image-17.jpg)

Finish sketch and extrude. Select the plane as well as the gap between joints, extrude “- cardboard” as we want to fill the gaps. 

![extrude settings](image-18.jpg)

![Finish Pannel](image-19.jpg)

I stopped here to test it, and laser cutted the first panels. It proved successful! 

So I went on with the other panel 

![box bottom 3d drawing](image-24.jpg)

![cutted box bottom part](fingerjoint-0.jpg)

Then I was on fire and created a lid ! 

![lid of the box 2D](image-30.jpg)

![finished box](image-31.jpg)

Tadaaaah !!!! 

![lid laser cutted](fingerjoint-1.jpg)

![finished box](fingerjoint-2.jpg)

So proud ! 


### From Fusion to illustrator 

First attempt, The mess… 

![first attempt](image-20.jpg)

Too many lines here ! 

Kris showed me a better method. 

Go on the main component, and select it. 

Make visible only the desired component to work on. 

Create a new sketch 

Then in the **create** menu choose **project/include** / **project**

![project sketch](illustrator-0.jpg)

Make invisible the source component to check the sketch and tune the line if necessary.

Finish sketch 

Then left click on the sketch name, and click on **save as TXL**

![project sketch](illustrator-1.jpg)

On illustrator open using **“importation”** (french) then choose the file.
Illustrator window opens, and choose **scale** 100%

![first attempt](illustrator-2.jpgjpg)

There you go ! 

## WTF is this? 

>>WTF is “project”

>>>>“They allow us to project a sketch onto a complex surface, following its profile.”

source: (https://fusion360tutorials.com/2020/04/25/fusion-360-project-to-surface-tool/)

>>WTF is “kerf”

>>>> It is litteraly a slit made by cutting with a saw. Here in our case the slit made but the laser burn. We need to take it in account if we want a perfectly fitting joinery. 

## Pickles 

Hmm... Constraint gone mad ! 

![Constraint gone mad](image-21.jpg)

Several comments from "overconstraint" and odd result of shapes. I understood to be mindful of the ESC key to make sure I don't mistakenly randomly click and constraint something... 

Also I understood all the constraints have little pictogram symbols. We can select these and delete them. 

I learn how to recognize which symbol is what to verify if the constraints are what I was planning, or if I just continuously made some random misfortunate clicks. 

Here ise some creativity... just for fun. 

![Constraint gone mad](image-25.jpg)

![Constraint gone mad](image-26.jpg)

I had plenty of those and struggled very much! 
