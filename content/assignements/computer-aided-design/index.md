+++
title= "Computer aided design"
+++

[About CAD](#about-cad)

[Basics](#basic-rules)

[Solvespace](#solvespace)
>[Basic commands](#basic-commands) 
>[On my own](#on-my-own) 

[Fusion 360](#fusion)
>[On my own](#on-my-own-1)
>[With the tutorial](#with-the-tutorial)

[WTF is this?](#wtf-is-this)





## About CAD 

>>What is CAD 

>>>"Computer aided design (CAD) is the use of computers (or workstations) to aid in the creation, modification, analysis, or optimization of a design. This software is used to increase the productivity of the designer, improve the quality of design, improve communications through documentation, and to create a database for manufacturing.Designs made through CAD software are helpful in protecting products and inventions when used in patent applications. CAD output is often in the form of electronic files for print, machining, or other manufacturing operations."

>>>Source (https://en.wikipedia.org/wiki/Computer-aided_design)


In a order to be clear with the steps and needs, prior to any CAD, it seems clear that we should prepare a rough hand sketch with all the measurements and think of the object and its possible symmetries, it’s possible equal parts and draw its minimal planes accordingly to use efficiently the CAD features.

We should imagine first the drawing in 2D, which would often be the first basic step of CAD before giving the shape a volume. 


## Solvespace 

[download here](https://github.com/solvespace/solvespace/releases/tag/v3.1)

### Basic rules 

You can draw both in 2D and 3D.

We can start drawing the basic geometry of the flat surface in 2D, then give the object a dimension by **extruding**. 
The logic is **constraining** the lines and the points to their geometrical parameter (horizontal, vertical, symmetrical, equal etc..). You can built construction lines. Create symmetrical repetitions. Work from one plane of the object to the other, creating separatly several part of an object. and connect them to each other later on. 

Toggle main plane to the centre point, from the point from which the symmetry should be performed. 

![Main center point](solvespace-center.jpg)


Each drawn objects needs to end up with 0 degrees of freedom, fully constraint before extruding. 

Beware of that when creating symmetrical repetition. 

![Symmetry before constraining](solvespace-symmetry.jpg)

![Symmetry before constraining](solvespace-constrained-symmetry.jpg)

### Basic commands 

>> **h** 

>>>> Constraint with horizontal 

>> **v** 

>>>> Constraint with vertical 

>> **d** 

>>>> Open the dimension box. Type dimension 

>> **o** 

>>>> Drag to point. ex: Use to drag the point of desired symmetry to the center of the pattern.


>> **e** 

>>>> Extrude. Open the dimension box, and enter the parameter of thickness. You can then choose one side, or two sides, depending on if you would like an asymmetrical or a symmetrical object. 

>>>>Several options It is possible to extrude objects: use for 

>> **repeat rotating tool** 

>>>> create a symmetrical repetition in a circular pattern from the center point. You can choose the number of repeats. 

>> **create a new plane and a new group tool**

>>>> Select a point and two perpendicular lines from which you want the plane to be related to, then click the new group tool. 

![New plane](solvespace-anchor%20plane.jpg)

>> **new point tool**

>>>> Create a new point, you can then constrain it on the desired line, with the **on line** command **o**. You can use it to determine on new anchor point in the middle of the line with the command **m** to the middle. 


>> **assembling tool**

>>>> Once all parts are done individually, we can assemble them in one. We will need to do it one by one. Choose the file to open. And constrain the object in the proper axe. 

*This is the actual speed of my brain, at the moment. I couldn't see where Kris second target*

### On my own 

Trying to make a very simple shape representing the leather strap for the step mechanism on the top of the mecanics machine. 

![First attempt](spimage-0.jpg)

![anchor attempt](spimage-1.jpg)

After several attempt to a logical construction I decided on this. 

![Starting point](spimage-2.jpg)

![extrude](spimage-3.jpg)

Successfully extruded the shape. The circles appear hollow which is what I need.

![multiply](spimage-4.jpg)

Then multiplied it, to keep only one repeat. I anchored the repeat with its twin. 
And that is what I need for now. 

![multiply](spimage-5.jpg)

It is nothing fancy or complex.

LOGIC HAS NEVER BEEN MY STRONG SUIT! Easy is always complicated. 


## Fusion 

After several experiementations and some very fast paced tutorial. 

I will try to create the shape experimenting with my poor knowledge first, then I will follow a tutorial on gear, to guide me to the proper steps. 

### On my own 


>Step one 

Create the structure of the wheel on a 2D plane

![2D plan](fusion-stepone.jpg)

>Step two 

Extrude the shape to have a 3D object. 

![Extrude wheel shape](fusion-steptwo.jpg)

Here weirdly he already created the hollow inside that I wanted. There are no constraints though... I wonder if this will be allright. 

>Step three

Now what... 
Hmmm...
Logically I would like to hollow one side in a segment. 

I decided to create a new component that I can merge later on? Maybe... 

![drew opening shape](fusion-stepthree.jpg)


Here I created a rectangle anchored to the center. I will try to to create a symmmetry just for fun. Of course I could already create one rectangle to the proper place and measures... 

![drew opening shape](fusion-stepthreebis.jpg)

![Finished rectangular shape](fusion-stepfour.jpg)


>Step four

I placed the section to their proper size and created a constraint to lock the shape. 

>Step five 

Now I need to extude the shape. 
Interesting, my symmetrical construction could help to extrude only one side of the rectangle. But let's select both objects... 

Investigating the extrude menu I can see there might be several legit ways I could negatively extrude the shape from the base object. 

But here I see this green arrow... I could probably direct it to the inner direction. YES ! 
I just need to give it a negative coordonate and then... select the cut operation... 

![extrude negatively](fusion-stepfive.jpg)

![extrude negatively cut menu](fusion-stepbis.jpg)hu

>Step six 

YEAH... 

![well done](fusion-stepsix.jpg)

![well done](fusion-stepsixbis.jpg)

>Step seven 

My instinct tells me that those parts are not joint and I would need to relate them somehow... 

![detached parts](fusion-stepseven.jpg)

But I will stop here from now... Because the assemble menu is a bit too intense. 

![link parts](fusion-stepsevenbis.jpg)



### With the tutorial 

>Step one 

Create the 2D sketch but this as before... First difference 

![link parts](image-0.jpg)

**Finish sketch** button... ahhhh, right

>Step 2 

The extrusion phase is using more features: 

Change the direction to symmetric and select the **whole lenght** mesurements button. 

![link parts](image-1.jpg)

Looks better with the proper measurements for my objects 

![link parts](image-2.jpg)

>Step 3 

Drawing the inside hollow features 

![link parts](image-3.jpg)

>Step 4

Use the **e** for extrude command, choose the **cut** operation, direct a bit towards the inside of the shape and modify **extend type** to **all**. 

![link parts](image-4.jpg)

![link parts](image-5.jpg)

![link parts](image-6.jpg)

YEAH! 

>Step 5

add details

![link parts](image-7.jpg)

Make it a bit more pretty, again using the extrude **cut** feature.

![link parts](image-9.jpg)

Using the **mirror** select **feature**, click **mirror plane** and choose **optimized** to reproduce on the other side. 

And that is it! 

![link parts](image-8.jpg)

Well not so far from what I have been trying... 

I tried learning from these tutorials: 

{{< youtube d3qGQ2utl2A >}}

But Gabriela found those, and they are more detailed and slow, good for an art student. Plus... Grand pa Paul is corny and endearing.

{{< youtube QX5HkmDH0f0 >}}

My preference goes to using fusion. 

I find it easier. The resource is very abundant. And it seems to have a plethora of features that I will probably never explore. But let's see if we can become friends... 

## WTF is this? 

**wtf is a constraint** 
>>>"A constraint in computer-aided design (CAD) software is a limitation or restriction imposed  upon geometric properties of an entity of a design model that maintains its structure as the model is manipulated."

source (https://en.wikipedia.org/wiki/Constraint_(computer-aided_design)

**wtf is a fillet** 
>>>The Fillet command rounds the edges of a solid body in Fusion 360 by removing material from exterior edges or adding material to interior edges. Design > Solid > Modify > Fillet.

source (https://help.autodesk.com/view/fusion360/ENU/?guid=SLD-REF-FILLET)



