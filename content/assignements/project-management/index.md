+++
title= "Project management"
+++



# Week One, Two and Three


[Git Bash](#git-bash)

[Git basic commands](#bash-basic-commands)

[In situation](#in-situation)

[Basic bash commands](#bash-basic-commands)

[HTML](#html)

[Gitlab](#gitlab)

[Hugo](#hugo)

[Image Magick](#magick)

[Wtf is this? Glossary](#wtf-is-this)


### Git Bash

What is Git?
Git is version-control system for tracking changes in source code during software development.

It is designed for coordinating work among programmers, but it can be used to track changes in any set of files.
Its goal is to increase efficiency, speed and easily manage large projects through version controlling.
Every git working directory is a full-fledged repository with complete history and full version-tracking capabilities, independent of network access or a central server.

Git helps the team cope up with the confusion that tends to happen when multiple people are editing the same files.
Git Bash is an application for Microsoft Windows environments which provides an emulation layer for a Git command line experience. Bash is an acronym for Bourne Again Shell. A shell is a terminal application used to interface with an operating system through written commands. Bash is a popular default shell on Linux and macOS. Git Bash is a package that installs Bash, some common bash utilities, and Git on a Windows operating system.

Source:(https://www.geeksforgeeks.org/working-on-git-bash/)

[How to get started on Git](https://git-scm.com/book/en/v2/Getting-Started-First-Time-Git-Setup)

My home directory: 

![Home directory](git-home-directory.jpg)

--------

Run the **Git config** command 

>**$ git config --global user.name "John Doe"**

>**$ git config --global user.email johndoe@example.com**

------

## Basic commands 

### git init

"git init turns any directory into a Git repository.

What Does git init Do?
git init is one way to start a new project with Git. 

To initialize a repository, Git creates a hidden directory called .git. That directory stores all of the objects and refs that Git uses and creates as a part of your project's history. This hidden .git directory is what separates a regular directory from a Git repository.

Common usages and options for **git init**
>**git init**

>>>Transform the current directory into a Git repository

>**git init directory**

>>>Transform a directory in the current path into a Git repository

*Source (https://github.com/git-guides)*

Here is the directory I elected for my project

![Home directory](git-work-directory.jpg)

### git add

The git add command adds new or changed files in your working directory to the Git **staging area.**

Common usages and options for git add
>**git add <path>** 

>>>Stage a specific directory or file

>**git add .**  

>>>Stage all files (that are not listed in the .gitignore) in the entire repository

>**git add filename/ filaname/ filename/**  

>>>Stage a selection of multiple files 

*Source (https://github.com/git-guides)*

### git commit

**git commit** creates a commit, which is like a snapshot of your repository. These commits are snapshots of your entire repository at specific times. You should make new commits often, based around logical units of change. Over time, commits should tell a story of the history of your repository and how it came to be the way that it currently is. Commits include lots of metadata in addition to the contents and message, like the author, timestamp, and more.

Common usages and options for Git Commit

>**git commit**

>>> This starts the commit process, but since it doesn't include a **-m** flag for the message, your default text editor will be opened for you to create the commit message.

>**git commit -m "descriptive commit message"**: 

>>>This starts the commit process, and allows you to include the commit message at the same time.

>**git commit -am "descriptive commit message"**

>>>In addition to including the commit message, this option allows you to skip the staging phase. The addition of -a will automatically stage any files that are already being tracked by Git (changes to files that you've committed before).

*Source (https://github.com/git-guides)*

### git status 

**git status** shows the current state of your Git working directory and staging area.
A useful feature of git status is that it will provide helpful information depending on your current situation. In general, you can count on it to tell you:

Where HEAD is pointing, whether that is a branch or a commit (this is where you are "checked out" to)
If you have any changed files in your current directory that have not yet been committed
If changed files are staged or not
If your current local branch is linked to a remote branch, then git status will tell you if your local branch is behind or ahead by any commits

Common usages and options for git status

>**git status** 
>>>Most often used in its default form, this shows a good base of information

>**git status -s** 
>>>Give output in short format

>**git status -v** 
>>>Shows more "verbose" detail including the textual changes of any uncommitted files

*Source (https://github.com/git-guides/git-status)*

## wtf is this

**wtf is a repository** 
>>>A Git repository is the .git/ folder inside a project. This repository tracks all changes made to files in your project, building a history over time. Meaning, if you delete the .git/ folder, then you delete your project’s history.

source(https://www.gitkraken.com/learn/git/tutorials/what-is-a-git-repository)

**wtf is a branch** 
>>>In Git, a branch is a new/separate version of the main repository.

>>>Branches allow you to work on different parts of a project without impacting the main branch. When the work is complete, a branch can be merged with the main project. You can even switch between branches and work on different projects without them interfering with each other.

source(https://www.w3schools.com/git/git_branch.asp?remote=github)

**wtf is a SSH key** 
>>>Essentially, SSH keys are an authentication method used to gain access to an encrypted connection between systems and then ultimately use that connection to manage the remote system. SSH keys always come in pairs, and every pair is made up of a private key and a public key. Who or what possesses these keys determines the type of SSH key pair. 

>>>If the private and public keys are on a remote system, then this key pair is referred to as host keys. 

source(https://jumpcloud.com/blog/what-are-ssh-keys)

**wtf is markdown**


**wtf are shortcodes**

{{< youtube 2xkNJL4gJ9E >}}



---------

## In situation

I used the **git add** command to place certains files in the staging area before a commit: 

![git add attempt](git-add-mistake.jpg)

Here the **status command** shows a list of files created and change in green. Which means they are in the staging area, ready to commit. 

It also shows a file in red. That means this file was also modified and I forgot to add it to the staging area. 

![git add attempt](git-commit-ex.jpg)


{{< youtube GFyjlPa73xU >}}

----------

## Bash Basic commands 

#### cd command 
**cd** command refers to **change directory** and is used to get into the desired directory.

>**cd /c/users/elise/folder_name/** 
>>>To navigate to a specific folder

>**cd ..**
>>>Navigate in one folder up. 

>**cd ../..**
>>>Navigate in two folder up. 

>**cd ~**
>>>Go back in the home directory.

#### ls command 
**ls** command is used to list all the files and folders in the current directory.

>**ls** 

![Git ls](git-ls.jpg)

Here the folder syntax is foldername/

The file syntax is filename.filetype

#### mkdir command 
**mkdir** command will create a new file within the specific space you are in. 

>**mkdir filename** 


#### ctrl + c shortcut / y

**ctrl + c** is usually a shortcut for copy past. But in Git it is a shotcut for escape! 

>**ctrl + c** or **y**
>>>Escape the current action. Use for escaping hugo server or magick.

----------

### HTML

#### What is HTML? 

HTML stands for Hyper Text Markup Language
HTML is the standard markup language for creating Web pages
HTML describes the structure of a Web page
HTML consists of a series of elements
HTML elements tell the browser how to display the content
HTML elements label pieces of content such as "this is a heading", "this is a paragraph", "this is a link", etc.

source(https://www.w3schools.com/html/html_intro.asp)

Many Html templates are available for free.

Most of the websites have some html code visible.

On chrome open the development tool **ctrl + shift + i**

You can copy some items. 

#### What is an HTML Element?

An HTML element is defined by a start tag, some content, and an end tag:

![html tags](html-tag.jpg)

source(https://www.w3schools.com/html/html_intro.asp)

Rooky mistakes: forgetting the "closing dash"

![Forgetting dash](html-mistake-dash.jpg)


![Correct dash](html-correct-dash.jpg)


### Creating basic HTML website 

We need: 

>> A code editor: **visual studio code**

>> A Html script: we can write it from scratch, copy it from a template ot ask chat GPT to generate it. 

[HTML cheat shit](https://htmlreference.io/element/video/)

----------

### Gitlab 

What is GitLab and why it is used?
GitLab is an open source code repository and collaborative software development platform. GitLab is free for individuals. GitLab offers a location for online code storage and capabilities for issue tracking and CI/CD.

Once the account open, create a new project

![New project](gitlab-newproject.jpg)


#### create a remote repository 

Kris did it for me. 

I had made a weird mess... 


#### create a SSH key 

Kris did it for me... 

I need to research it now I am not in the panic of it anymore.


### Hugo

Kris also installed hugo for me and created a new website.

>>>>>>**lack documentation here, need more research**


>>**hugo serve**

>>> Within the directory where the website is constructed hugo can open a server. It will run an emulation of the website while writting it down.

![Hugo serve](hugo-serve.jpg)

An address has been created on which we can follow the changes we are building in real time. 

#### Push Hugo Website to Gitlab 

Go to **gitlab directory**

Create a **new file** specifying **hugo** template

![Code Hugo yml](lines-integration-hugo.jpg)
It will produce a code for rendering Hugo website through Gitlab 

**Copy** this code 
**Paste it** in the .gitlab yml file in **Visual Studio Code**

IN GITBASH
>> **git commit -m "add CI config to compile and publish Hugo site"** 

>> **git push**

------

#### Create a video markdown using html shortcodes

![markdown for a video](code-video-markdown.jpg "Title here")

Structure of markdown for a video

![example of video markdown script](ex-video-markdown.jpg)

Example of video markdown script


>>In **layouts** create a folder called **shortcodes**
>>>Then a file called **video.html**

[We need a html script for a video tag ](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/video)

![html video tag](code-video-html.jpg)

Then simplifying it

![Simplified code](code-video-simple-html.jpg)

You can tune size, specify the format. 


----------

### Magick 

#### Installing Magick 



Invoke Magick in GitBash 

![Magick](invoke-magick.jpg)

Magick needs to be invoke in the directory where the image to modify are. 

#### Commands 

>**magick -h** 
>>>for HELP section 

>**magick identify**
>>>describe the format and characteristics of one or more image files.


[Here find all the available tools ](https://imagemagick.org/script/command-line-tools.php)

>**magick convert**
>>>Convert between image formats as well as resize an image, blur, crop, despeckle, dither, draw on, flip, join, re-sample, and much more.

>>>*ex: magick convert imagename.jpg -resize 800x500 image_800x500.jpg

>>>>>Here 5 sections of information 
>>>>>**magick convert** command 

>>>>>file name: **imagename.jpg**

>>>>>the value -**resize**

>>>>>then specify resizing size **800x500**

>>>>>specify new name of file: **image_800x500.jpg**


[Here find all possible values](https://imagemagick.org/script/convert.php)




